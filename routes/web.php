<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('candidates', 'CandidatesController')->middleware('auth');


Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');
Route::get('candidates/departments/{did}', 'RegisterController@departments')->name('candidates.departments')->middleware('auth');

//Route::get('/user.candidates','CandidatesController@myCandidates')->name('user.candidates')->middleware('auth');
Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');



Route::get('/', function(){
    return view('welcome');
});


Route::get('/hello',function(){
    return 'Hello Laravel';

});

Route::get('/student/{id}',function($id = 'No student found'){
    return 'We got student id '.$id ;

});

Route::get('car/{id?}',function($id = null){
    if(isset($id)){
        //TODO: validate your integer
        return "we got car $id";
    } else{
        return 'we need the id to find your car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});

//Home Ex. 5

Route::get('/users/{email}/{name?}',function($email = null,$name = null){
    return view('users',compact('email','name'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
