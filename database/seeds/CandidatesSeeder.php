<?php

use Illuminate\Database\Seeder;
use Carbon\carbon;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
            'name' => Str::random(10),
            'email'=> Str::random(10).'@gmail.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
    }
}
